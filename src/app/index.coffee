angular.module "geoffwhatley", ['ngAnimate', 'ngCookies', 'ngSanitize', 'ngResource', 'ngRoute']
  .config ($routeProvider) ->
    $routeProvider
      .when "/",
        templateUrl: "app/main/main.html"
        controller: "MainCtrl"
      .otherwise
        redirectTo: "/"

